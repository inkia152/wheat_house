    $(document).ready(function() {
    var curPage = document.URL.substr(document.URL.lastIndexOf('/') + 1);

    $('.h-nav__item[data-name="' + curPage + '"]').addClass('h-nav__item--active')

	$('.carousel-wrapper').slick({
        infinite: true,
        speed: 1000,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 5000,
        swipe: false,
        arrows: false
	})

    $('.proj-slider').slick({
        infinite: true,
        speed: 1000,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: true,
        nextArrow: '<button class="slick-arrow slick-next">&rsaquo;</button>',
        prevArrow: '<button class="slick-arrow slick-prev">&lsaquo;</button>'
    });

    $('.burger').click(function(){
        $(this).toggleClass('open');
        $('.slide-menu').toggleClass('active-menu');
        $('.main').toggleClass('slide-page');
    });

    $('.projects__item').click(function() {
        $('.project-full').addClass('growup')
        $('.shadow').addClass('shadow-active')
    });

    $('.shadow').on('click', function() {
        $('.project-full').removeClass('growup')
        $('.shadow').removeClass('shadow-active')
    });

    $('.cross').on('click', function() {
        $('.project-full').removeClass('growup')
        $('.shadow').removeClass('shadow-active')
    });



    // animation

    $('.header__logo').animated('slideInLeft');
    $('.header__nav').animated('slideInRight');
    $('.bi-item').animated('slideInUp');
    $('.ii-item').animated('zoomIn');
    $('.ti-item').animated('fadeInUp');
    $('.text-info__main-title').animated('zoomIn');
    $('.concept__text').animated('slideInUp');
    $('.concept__img').animated('slideInUp');
    $('.projects__item').animated('zoomIn');
    $('.services__item:nth-of-type(1)').animated('fadeInDown');
    $('.services__item:nth-of-type(2)').animated('fadeInUp');
    $('.social__item').animated('flipInY');
    $('.input-wrapper label').animated('fadeInDown');
    $('.contact__textarea-wrapper').animated('fadeInLeft');
    $('.contact__button').animated('fadeInUp');
    // $('.footer__copyr .left').animated('slideInLeft');
    // $('.footer__copyr .right').animated('slideInRight');
});