from django.contrib import admin
from wheat_site.models import Contact, Project, Image

class ImageInline(admin.TabularInline):
    model = Image

# admin.site.register(Project)


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    inlines = [ImageInline]


admin.site.register(Contact)
admin.site.register(Image)
