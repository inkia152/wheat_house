from django.apps import AppConfig


class WheatSiteConfig(AppConfig):
    name = 'wheat_site'
