from django import forms


class ContactForm(forms.Form):
    name = forms.CharField(max_length=24, widget=forms.TextInput(attrs={"class": "contact__item"}))
    email = forms.EmailField(max_length=64, widget=forms.TextInput(attrs={"class": "contact__item"}))
    phone = forms.CharField(min_length=8, widget=forms.TextInput(attrs={"class": "contact__item"}))
    comment = forms.CharField(max_length=256, widget=forms.Textarea(attrs={"cols": "30",
                                                                           "rows": "12",
                                                                           "class": "contact__textarea"}))