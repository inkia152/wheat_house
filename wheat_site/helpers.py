# coding: utf-8
from wheat_site.models import Image


def get_carousel_paths():
    """
    :return: list of paths to file
    """
    return [path[0] for path in Image.objects.filter(carousel=True).values_list('path')]
