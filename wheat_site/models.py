import datetime as dt
from wheat_house.settings import MEDIA_PATH
import os

from django.db import models
from django.db.models import CharField, DateTimeField, ForeignKey, BooleanField, ImageField
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFit


def upload_to(instance, filename):
    """
    Auto generate name for File and Image fields.
    :param instance: Instance of Model
    :param filename: Name of uploaded file
    :return:
    """
    return os.path.join('projects', str(instance.project_id.id), filename)


def upload_to_thumb(instance, filename):
    return os.path.join('projects', str(instance.id), "thumb_" + filename  )


class Contact(models.Model):
    name = CharField(max_length=24, null=False)
    date = DateTimeField(auto_now_add=True, blank=True)
    email = CharField(max_length=64)
    phone = CharField(max_length=24, null=False)
    description = CharField(max_length=1024)

    class Meta:
        verbose_name = 'Контакт'
        verbose_name_plural = 'Контакты'

    def __repr__(self):
        return '<{}>: {}'.format(self.__class__.__name__,
                              self.id)


class Project(models.Model):
    date = DateTimeField(default=dt.datetime.now, blank=True)
    name = CharField(max_length=128, null=False)
    title = CharField(max_length=1024, null=True)
    description = CharField(max_length=1024)
    thumbnail = ProcessedImageField(upload_to=upload_to_thumb, processors=[ResizeToFit(300)], format='JPEG',
                                    options={'quality': 100})

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'

    def save(self, *args, **kwargs):
        if self.id is None:
            saved_image = self.thumbnail
            self.thumbnail = None
            super(Project, self).save(*args, **kwargs)
            self.thumbnail = saved_image
            if 'force_insert' in kwargs:
                kwargs.pop('force_insert')

        super(Project, self).save(*args, **kwargs)

    def __repr__(self):
        return '<{}>: {}'.format(self.__class__.__name__,
                              self.id)


class Image(models.Model):
    image = ProcessedImageField(upload_to=upload_to, processors=[ResizeToFit(1280)], format='JPEG',
                                options={'quality': 100})
    carousel = BooleanField(default=False)
    project_id = ForeignKey(Project, null=False, on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'
    def __repr__(self):
        return '<{}>: {}'.format(self.__class__.__name__,
                              self.id)