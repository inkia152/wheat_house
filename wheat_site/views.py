from django.shortcuts import render

from wheat_site.forms import ContactForm
from wheat_site.models import Contact, Project, Image


def getspio(request):
    return render(request, 'gestpio/index.html')


def index(request):
    carousel_paths = ["media/" + path[0] for path in Image.objects.filter(carousel=True).values_list('image')]
    return render(request, 'index.html', {'carousel_paths': carousel_paths})


def concepts(request):
    return render(request, 'concepts.html')


def projects(request):
    projects = Project.objects.all()
    for project in projects:
        print(project.thumbnail)
    return render(request, 'projects.html', {'projects': projects})


def project(pk):
    project = Project.objects.filter(pk=pk).first()
    return project


def services(request):
    return render(request, 'services.html')


def contacts(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            clean = form.cleaned_data
            contact = Contact(name=clean['name'],
                              email=clean['email'],
                              phone=clean['phone'],
                              description=clean['comment'])
            contact.save(
            )
            form = ContactForm()
            return render(request, 'contacts.html', {'form': form, 'message': 'success'})
    else:
        form = ContactForm()
    return render(request, 'contacts.html', {'form': form})